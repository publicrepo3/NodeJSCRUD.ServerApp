import express from "express";
import authRouter from "./routes/auth/auth_router";
///
const serverApp = express();
serverApp.use(express.json());
serverApp.use("/RealstateServerApp/", authRouter);
export default serverApp;
