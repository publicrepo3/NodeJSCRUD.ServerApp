import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import { ResponseStatus, ResponseUtil } from "../utils/general_utils";

export default class GeneralMiddleWares {
  static handleValidationError(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const error = validationResult(req);
    if (!error.isEmpty())
      return ResponseUtil.getResponse(
        res,
        {
          errors: error.array(),
        },
        ResponseStatus.forbiddenStatus
      );
    next();
  }
}
