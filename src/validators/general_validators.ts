import { body, param, query } from "express-validator";
import GeneralParams from "../params/general_params";
import GeneralValidatorMessages from "./general_validator_messages";

class AuthValidator {
  static checkLogin() {
    return [
      body(GeneralParams.userEmail)
        .notEmpty()
        .withMessage(GeneralValidatorMessages.mustBeNotEmpty)
        .isString()
        .withMessage(GeneralValidatorMessages.mustBeString),
      body(GeneralParams.userPassword)
        .notEmpty()
        .withMessage(GeneralValidatorMessages.mustBeNotEmpty)
        .isString()
        .withMessage(GeneralValidatorMessages.mustBeString)
        .isStrongPassword()
        .withMessage(GeneralValidatorMessages.mustBeStrongPassword),
    ];
  }
}
export { AuthValidator };
