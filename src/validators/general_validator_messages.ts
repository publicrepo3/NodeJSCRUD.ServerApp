export default class GeneralValidatorMessages {
  static mustBeString = "Must be string value";
  static mustBeNotEmpty = "Empty values is forbidden";
  static mustBeStrongPassword = "Must be strong password";
}
