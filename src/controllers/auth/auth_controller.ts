import { Request, Response } from "express";
import GeneralParams from "../../params/general_params";
import { ResponseStatus, ResponseUtil } from "../../utils/general_utils";

export default class AuthenticationController {
  static async loginUser(req: Request, res: Response) {
    try {
      const finalResponse = req.body;
      delete finalResponse[GeneralParams.userPassword];
      ResponseUtil.getResponse(res, req.body, 200);
    } catch (error) {
      ResponseUtil.getResponse(
        res,
        {
          message: error,
        },
        ResponseStatus.exceptionStatus
      );
    }
  }
}
