import express from "express";
import AuthenticationController from "../../controllers/auth/auth_controller";
import GeneralMiddleWares from "../../middlewares/general_middleware";
import { AuthValidator } from "../../validators/general_validators";
import GeneralValidatorMessages from "../../validators/general_validator_messages";
import GeneralRouter from "../general_router";

///

const router = express.Router();

///LoginRoute
/// [00:34:03] is explained
router.post(
  ///Route
  GeneralRouter.authRouter.logInRoute,
  ///Validators
  AuthValidator.checkLogin(),
  ///(req,res,next)=>{}
  GeneralMiddleWares.handleValidationError,
  ///(req,res)=>{}
  AuthenticationController.loginUser
);
///
export default router;
