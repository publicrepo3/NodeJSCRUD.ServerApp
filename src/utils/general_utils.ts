import { Request, Response } from "express";
class GeneralResponse {
  status: number;
  data: {};
  constructor(_status: number, _data: {}) {
    this.status = _status;
    this.data = _data;
  }
}
export class ResponseUtil {
  static getResponse(res: Response, data: {}, status: number) {
    res.status(status);
    res.json({
      status: status === 200,
      response: data,
    });
  }
}

export class ResponseStatus {
  static successStatus = 200;
  static exceptionStatus = 400;
  static forbiddenStatus = 405;
}
