export default class GeneralParams {
  ///GeneralParams
  static message = "message";
  static errorMessage = "errorMessage";
  ///InheritedFromAuthParams
  static userEmail = "userEmail";
  static userPassword = "userPassword";
}
