import express, { Request, Response } from "express";
import serverApp from "./app";
///

const port: number = 9000;
serverApp.listen(port, () => {
  console.log("Server is running on :" + port);
});
